angular
    .module('RDash')
    .factory('PropostaDadosSrv', PropostaDadosSrv);

PropostaDadosSrv.$inject = ['config', '$http', '$filter'];
function PropostaDadosSrv(config, $http, $filter) {

    var _getPropostaDados = function (item, uri) {
        if (!uri) {
            uri = 'proposta-dados';
        }
        if(!item.coMaterial){
            item.coMaterial = '';
        }
        if(!item.nuSabesp){
            item.nuSabesp = '';
        }
        if(!item.coEmpresa){
            item.coEmpresa = '';
        }else{
            var dados = item.coEmpresa;
            item.coEmpresa = dados.coEmpresa;
        }
        if(!item.stMeusPrecos){
            item.stMeusPrecos = '';
        }
        if(!item.periodo){
            item.periodo = '';
        }
        if(!item.dtInicio){
            item.dtInicio = '';
        }else{
            item.dtInicio = $filter('date')(new Date(item.dtInicio),'yyyy-MM-dd');
        }
        if(!item.dtFim){
            item.dtFim = '';
        }else {
            item.dtFim = $filter('date')(new Date(item.dtFim),'yyyy-MM-dd');
        }
        return config.baseUrl + '/banco-preco/'+uri+'?search='+item.search+'&coMaterial='+item.coMaterial+
                '&nuSabesp='+item.nuSabesp+"&coEmpresa="+item.coEmpresa+"&stMeusPrecos="+item.stMeusPrecos+
                '&periodo='+item.periodo+"&dtInicio="+item.dtInicio+"&dtFim="+item.dtFim;
    };

    var _getPorCoPropostaDados = function (coPropostaDados) {
        return $http.get(config.baseUrl + '/banco-preco/proposta-dados/'+coPropostaDados);
    };

    var _savePropostaDados = function (propostaDados, coPropostaDados) {
        if (coPropostaDados) {
            return $http.put(config.baseUrl + '/banco-preco/proposta-dados/'+coPropostaDados, propostaDados);
        }
        return $http.post(config.baseUrl + '/banco-preco/proposta-dados', propostaDados);

    };

    var _updatePropostaDados = function (coPropostaDados, propostaDados) {
        return $http.put(config.baseUrl + '/banco-preco/proposta-dados/'+coPropostaDados, propostaDados);
    };

    var _deletePropostaDados = function (idPropostaDados) {
        return $http.delete(config.baseUrl + '/banco-preco/proposta-dados/'+idPropostaDados);
    };

    return {
        getPropostaDados: _getPropostaDados,
        getPorCoPropostaDados: _getPorCoPropostaDados,
        updatePropostaDados: _updatePropostaDados,
        savePropostaDados: _savePropostaDados,
        deletePropostaDados: _deletePropostaDados,
    };
}