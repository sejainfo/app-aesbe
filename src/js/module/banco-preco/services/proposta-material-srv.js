angular
    .module('RDash')
    .factory('PropostaMaterialSrv', PropostaMaterialSrv);

PropostaMaterialSrv.$inject = ['config', '$resource'];

function PropostaMaterialSrv(config, $resource) {

    var _getArquvoProposta = function () {
        return $resource(
            config.baseUrl + '/banco-preco/arquivo-proposta'
        ).get();
    };

    return {
        getArquvoProposta: _getArquvoProposta
    };
}
