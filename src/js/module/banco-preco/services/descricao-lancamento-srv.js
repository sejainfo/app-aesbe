angular
    .module('RDash')
    .factory('DescricaoLancamentoSrv', ['$http', 'config', DescricaoLancamentoSrv]);

function DescricaoLancamentoSrv($http, config) {

    var _getDescricaoLancamento = function () {
        return $http.get(config.baseUrl + '/banco-preco/descricao-lancamento');
    };

    return {
        getDescricaoLancamento: _getDescricaoLancamento
    };


}