angular
    .module('RDash')
    .factory('ModalidadeLicitacaoSrv', ModalidadeLicitacaoSrv);

ModalidadeLicitacaoSrv.$inject = ['$http', 'config'];
function ModalidadeLicitacaoSrv($http, config) {

    var _getModalidadeLicitacao = function () {
        return $http.get(config.baseUrl + '/banco-preco/modalidade-licitacao');
    };
    return {
        getModalidadeLicitacao: _getModalidadeLicitacao
    };
}
