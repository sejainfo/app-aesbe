angular
    .module('RDash')
    .factory('RelatorioSrv', RelatorioSrv);

RelatorioSrv.$inject = ['config', '$resource', '$http'];

function RelatorioSrv(config, $resource, $http) {

    var _enviarRelatorioPorEmail = function (listaEmail, item) {
        var post = {
            listaEmail: listaEmail,
            itemPesquisa: item
        }

        return $http.post(config.baseUrl + '/banco-preco/relatorio', post);
    };

    return {
        enviarRelatorioPorEmail: _enviarRelatorioPorEmail
    };
}
