angular
    .module('RDash')
    .factory('MaterialSrv', MaterialSrv);

MaterialSrv.$inject = ['$http', 'config'];
function MaterialSrv($http, config) {

    var _getMaterial = function () {
        return $http.get(config.baseUrl + '/banco-preco/material');
    };

    return {
        getMaterial: _getMaterial
    };


}