angular
    .module('RDash')
    .factory('FornecedorSrv', FornecedorSrv);

FornecedorSrv.$inject = ['$http', 'config'];

function FornecedorSrv($http, config) {

    var _getFornecedores = function () {
        return $http.get(config.baseUrl + '/banco-preco/fornecedor');
    };

    return {
        getFornecedores: _getFornecedores
    };


}
