/**
 * Alerts Controller
 */

angular
    .module('RDash')
    .controller(
        'PropostaMaterialListarCtrl',
        [
            '$scope', '$controller', 'PropostaDadosSrv', 'inform', '$uibModal','MenuSrv','$localStorage',
            PropostaMaterialListarCtrl
        ]
);

function PropostaMaterialListarCtrl(
    $scope, $controller, PropostaDadosSrv, inform, $uibModal, MenuSrv, $localStorage
)
{
    angular.extend($controller('AbstractCtrl', {$scope: $scope}));
    var item = {
        "search": " ",
        "coEmpresa": "2",
        "stMeusPrecos": "true"
    };
    $scope.pesquisarPrecoMaterial(PropostaDadosSrv.getPropostaDados(item), true);


    $scope.openModalProposta = function (coPropostaDados, row) {
        $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'modalExcluirPropostaDados.html',
            controller: 'ModalExcluirPropostaDadosCtrl',
            scope: $scope,
            resolve: {
                coPropostaDados: coPropostaDados,
                row: row
            }
        });
    };
}
