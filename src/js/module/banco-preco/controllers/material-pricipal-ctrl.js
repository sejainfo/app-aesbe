/**
 * Alerts Controller
 */

angular
    .module('RDash')
    .controller(
        'MaterialPrincipalCtrl',
        [
            '$scope', '$stateParams', '$filter', '$controller', 'MaterialSrv',
            'DTOptionsBuilder', 'inform',
            MaterialPrincipalCtrl
        ]
);

function MaterialPrincipalCtrl(
    $scope, $stateParams, $filter, $controller, MaterialSrv, DTOptionsBuilder, inform
)
{
    angular.extend($controller('AbstractCtrl', {$scope: $scope}));
    var vm = this;
    vm.dtOptions = DTOptionsBuilder.newOptions()
        .withOption({
                "bPaginate": true,
                "bLengthChange": true,
                "bFilter": true,
                "bFixedHeader": true,
                "bSort": true,
                "bInfo": true,
                "bAutoWidth": true,
                "bStateSave": true
            }
        )
        .withPaginationType('simple_numbers')
        .withOption('responsive', true)
        .withLanguage({
            "sProcessing": "Processando...",
            "sLengthMenu": "Mostrar _MENU_ registros por página",
            "sZeroRecords": "Nenhum Registro Encontrado...",
            "sInfo": "Paginação _START_ de _END_, Total _TOTAL_ registros",
            "sInfoEmpty": "Paginação 0 de 0, Total 0 registros",
            "sInfoFiltered": "(Filtrado de _MAX_ registros ao todo)",
            "sSearch": "Procurar: ",
            "sNext": "Próximo",
            "sLoadingRecords": "carregando...",
            "sEmptyTable": "Nenhum Registro Encontrado",
            "oPaginate": {
                "sPrevious": "Anterior",
                "sNext": "Próximo",
                "sFirst": "Primeira",
                "sLast": "Última",
                "sZeroRecords": "Nenhum Registro Encontrado..."
            }
        });

    vm.load = function(){
        MaterialSrv.getMaterial()
            .success(function (data) {
            vm.registros = data._embedded.material;
        });

    };
    vm.load();

}
