/**
 * Alerts Controller
 */

angular
    .module('RDash')
    .controller(
        'PropostaMaterialPrincipalCtrl',
        [
            '$scope', '$resource', 'PropostaDadosSrv', '$timeout','$controller','EmpresaSrv','$window',
            'periodo', '$filter', '$uibModal',
            PropostaMaterialPrincipalCtrl
        ]
    );

function PropostaMaterialPrincipalCtrl(
    $scope, $resource, PropostaDadosSrv, $timeout, $controller, EmpresaSrv, $window, periodo, $filter, $uibModal
)
{
    angular.extend($controller('AbstractCtrl', {$scope: $scope}));
    $scope.dtInstance = {};
    $scope.pesquisa = false;
    $scope.stPeriodoPersonalizado = false;
    EmpresaSrv.getEmpresas().success(function (data) {
        $scope.empresas =  data._embedded.empresa
    });
    $scope.pesquisarPrecoMaterial();
    $scope.pesquisarMaterial = function (item) {
        $scope.dataSearch = $resource(PropostaDadosSrv.getPropostaDados(item)).query();
        $scope.dadosPesquisa = $scope.dataSearch;

        $scope.dtInstance.changeData($scope.dadosPesquisa.$promise);
        $scope.dtInstance.reloadData();
        $timeout(function(){
            $scope.pesquisa = true;
        }, 600);
    };

    $scope.exportar = function (item) {
        $window.location.href = PropostaDadosSrv.getPropostaDados(item, 'relatorio');
    }
    $scope.periodoPersonalizado = function (opcao) {
        if (periodo.PERSONALIZADO == opcao) {
            $scope.stPeriodoPersonalizado = true;
        } else{
            $scope.stPeriodoPersonalizado = false;
        }

    }

    $scope.animationsEnabled = false;

    $scope.open = function (size) {
        $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'modalEnviarEmailRelatorio.html',
            controller: 'ModalEnviarEmailRelatorioCtrl',
            size: size,
            scope: $scope
        });
    };
}
