/**
 * Alerts Controller
 */

angular
    .module('RDash')
    .controller(
        'PropostaMaterialFormularioCtrl',
        [
            '$scope', 'MaterialSrv', '$controller', '$stateParams', 'ModalidadeLicitacaoSrv',
            'PropostaDadosSrv', 'inform', '$location',
            PropostaMaterialFormularioCtrl
        ]
    );

function PropostaMaterialFormularioCtrl(
    $scope, MaterialSrv, $controller, $stateParams, ModalidadeLicitacaoSrv, PropostaDadosSrv, inform, $location
)
{
    angular.extend($controller('AbstractCtrl', {$scope: $scope}));
    $scope.itensMaterial = [];
    $scope.itensModalidade = [{
        "id": "",
        "label": "Selecione"
    }];

    var carregarMaterial = function () {
        MaterialSrv.getMaterial().success(function (data) {
            angular.forEach(data._embedded.material, function (item) {
                $scope.itensMaterial.push(
                    {
                        "id": item.coMaterial,
                        "name": item.coMaterial + ' - ' + item.noBasico +  ' - ' + item.noDescricao
                    }
                );
            });
        });
    };
    var carregarModalidadeLicitacao = function () {
        ModalidadeLicitacaoSrv.getModalidadeLicitacao().success(function (data) {
            angular.forEach(data._embedded.modalidade_licitacao, function (item) {
                $scope.itensModalidade.push({
                    "id": item.idDescLancamento,
                    "label": item.dsModalidade
                });
                $scope.modalidadeLicitacao = {
                    "values": $scope.itensModalidade
                };
            });
        });
    };
    carregarMaterial();
    carregarModalidadeLicitacao();
    $scope.getPropostaDados = function(){
        PropostaDadosSrv.getPorCoPropostaDados($scope.coPropostaDados)
            .success(function(data) {
                $scope.item = data;
                if ($scope.item) {
                    $scope.item.dtProposta = $scope.formatDate($scope.item.dtProposta);
                    $scope.item.dtContratacao = $scope.formatDate($scope.item.dtContratacao);
                    $scope.item.dtVigenciaContrato = $scope.formatDate($scope.item.dtVigenciaContrato);
                    $scope.item.dtInformacao = $scope.formatDate($scope.item.dtInformacao);
                    $scope.item.coMaterial = {
                        "id": $scope.item.materialEntity.coMaterial,
                        "name": $scope.item.materialEntity.coMaterial + ' - ' + $scope.item.materialEntity.noBasico +  ' - ' + $scope.item.materialEntity.descricaoEntity.noDescricao
                    }
                    $scope.item.noFornecedor = $scope.item.fornecedorEntity.noFornecedor;
                    $scope.item.nuCnpj = $scope.item.fornecedorEntity.nuCnpj;
                    $scope.item.coModalidade = $scope.item.modalidadeLicitacaoEntity.coModalidade + '';
                }
            });
    }
    if($stateParams.id){
        $scope.coPropostaDados = $stateParams.id;
        $scope.getPropostaDados();
    }
    $scope.propostaFormularioSubmit = function (itemProposta) {

        PropostaDadosSrv.savePropostaDados(itemProposta, $scope.coPropostaDados)
            .success(function() {
                inform.add("Preço atualizado com sucesso", {
                    "type": "success"
                });
                $location.path('/banco-preco/proposta-material/listar');
            })
            .error(function(data) {
                $scope.tratamentoErros(data);
            });
    };


}
