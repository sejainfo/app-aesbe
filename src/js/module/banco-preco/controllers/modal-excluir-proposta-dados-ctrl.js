angular
    .module('RDash')
    .controller('ModalExcluirPropostaDadosCtrl',
        [
            '$scope', '$uibModalInstance', 'PropostaDadosSrv', '$controller', 'inform','coPropostaDados','row',
            ModalExcluirPropostaDadosCtrl
        ]);

function ModalExcluirPropostaDadosCtrl($scope, $uibModalInstance, PropostaDadosSrv, $controller, inform, coPropostaDados, row) {
    angular.extend($controller('AbstractCtrl', {$scope: $scope}));
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.excluirPropostaDados = function(){
        PropostaDadosSrv.deletePropostaDados(coPropostaDados)
            .success(function() {
                $uibModalInstance.dismiss('cancel');
                $(row).remove();
                inform.add('Registro excluído com sucesso', {
                    "type": "success",
                    "ttl": 9000,
                });
            });
    };
}