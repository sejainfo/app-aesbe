angular
    .module('RDash')
    .controller(
        'PropostaMaterialListaImportadosCtrl',
        [
            '$scope', 'DTOptionsBuilder', '$controller',
            'DTColumnBuilder', '$filter', 'PropostaMaterialSrv',
            PropostaMaterialListaImportadosCtrl
        ]
    );

function PropostaMaterialListaImportadosCtrl(
    $scope, DTOptionsBuilder, $controller, DTColumnBuilder, $filter, PropostaMaterialSrv
)
{
    angular.extend($controller('AbstractCtrl', {$scope: $scope}));
    $scope.listaArquivoProposta = PropostaMaterialSrv.getArquvoProposta();
    $scope.dtColumns = [
        DTColumnBuilder.newColumn('arquivo').withTitle('Arquivo'),
        DTColumnBuilder.newColumn('dtProcessamento').withTitle('Data de Processamento'),
        DTColumnBuilder.newColumn('qtRegistros').withTitle('Quantidade de Registros').renderWith(function (quantidade, type) {
            return $filter('number')(quantidade);
        }),
        DTColumnBuilder.newColumn('qtErros').withTitle('Quantidade de Erros').renderWith(function (quantidade, type) {
            return $filter('number')(quantidade);
        }),
        DTColumnBuilder.newColumn('stProcessamento').withTitle('Arquivo Processado'),
    ];
    $scope.dtOptions = DTOptionsBuilder
        $scope.dtOptions = DTOptionsBuilder.fromFnPromise(function() {
            return $scope.listaArquivoProposta.$promise;
        })
        .withPaginationType('simple_numbers')
        .withOption($scope.option)
        //.withOption('responsive', true)
        .withDataProp('_embedded.arquivo_proposta')
        .withLanguage($scope.optionLanguage);
    //console.log($scope.listaArquivoProposta);
    //console.log($scope.listaArquivoProposta._embedded);
    //$scope.listaArquivoProposta = $scope.listaArquivoProposta._embedded.arquivo_proposta;
}
