/**
 * Alerts Controller
 */

angular
    .module('RDash')
    .controller(
        'PropostaMaterialImportarDadosCtrl',
        [
            '$scope', 'inform', '$controller','FileUploader','config','OAuthToken',
            PropostaMaterialImportarDadosCtrl
        ]
);

function PropostaMaterialImportarDadosCtrl(
    $scope, inform, $controller, FileUploader, config, OAuthToken
)
{
    angular.extend($controller('AbstractCtrl', {$scope: $scope}));
    var uploader = $scope.uploader = new FileUploader({
        url: config.baseUrl + '/banco-preco/arquivo-proposta',
        queueLimit: 1
    });
    var tokenInfo = OAuthToken.getToken();
    uploader.headers["Authorization"] = "Bearer " + tokenInfo.access_token;
    $scope.alerts = [];
    $scope.erros = [];
    $scope.complete = true;
    // FILTERS

    uploader.filters.push({
        name: 'customFilter',
        fn: function(item /*{File|FileLikeObject}*/, options) {
            var split = item.name.split('.');
            var extensao = split[1];
            if (!(extensao == 'xls' || extensao == 'xlsx')) {
                inform.add('Extensão de arquivo inválida. Somente permitido arquivos com extensões: .xls ou .xlsx', {
                    "type": "danger",
                    "ttl": 9000,
                });
                return false;
            } else {
                inform.remove();
            }


            //console.log(item.name = );
            //return this.queue = null;
            return this.queue.length < 10;
        }
    });


    // CALLBACKS

    uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {
        //console.info('onWhenAddingFileFailed', item, filter, options);
    };
    uploader.onAfterAddingFile = function(fileItem) {
        //console.info(fileItem.file.name);
    };
    uploader.onAfterAddingAll = function(addedFileItems) {
        //console.info('onAfterAddingAll', addedFileItems);
    };
    uploader.onBeforeUploadItem = function(item) {
        $scope.complete = false;
    };
    uploader.onProgressItem = function(fileItem, progress) {
        //console.info('onProgressItem', fileItem, progress);
    };
    uploader.onProgressAll = function(progress) {
        //console.info('onProgressAll', progress);
    };
    uploader.onSuccessItem = function(fileItem, response, status, headers) {
        inform.add("Arquivo importado com sucesso", {
            "type": "success"
        });

    };
    uploader.onErrorItem = function(fileItem, response, status, headers) {
        $scope.erros = response.erros;
        if (response.detail) {
            inform.add(response.detail, {
                "type": "danger",
                "ttl": 9000,
            });
        } else {
            inform.add("Arquivo não processado por conter erros", {
                "type": "danger",
                "ttl": 9000,
            });
        }

    };
    uploader.onCancelItem = function(fileItem, response, status, headers) {
        //console.info('onCancelItem', fileItem, response, status, headers);
    };
    uploader.onCompleteItem = function(fileItem, response, status, headers) {
        //console.info('onCompleteItem', fileItem, response, status, headers);
    };
    uploader.onCompleteAll = function() {
        $scope.complete = true;
        //console.log($scope.complete);
    };
    $scope.removerErros = function(){
        $scope.erros = [];
    }
}
