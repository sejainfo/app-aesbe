angular
    .module('RDash')
    .controller('ModalEnviarEmailRelatorioCtrl',
        [
            '$scope', '$uibModalInstance', 'RelatorioSrv', '$controller', 'inform',
            ModalEnviarEmailRelatorioCtrl
        ]);

function ModalEnviarEmailRelatorioCtrl($scope, $uibModalInstance, RelatorioSrv, $controller, inform) {
    angular.extend($controller('AbstractCtrl', {$scope: $scope}));
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
    $scope.eviarEmailRelatorioSubmit = function (listaEmail) {
        RelatorioSrv.enviarRelatorioPorEmail(listaEmail,$scope.item)
            .success(function(data) {
                inform.add('Relatório enviado por email com sucesso', {
                    "type": "success",
                    "ttl": 9000,
                });
            })
            .error(function(data) {
                inform.add(data.detail, {
                    "type": "danger",
                    "ttl": 9000,
                });
            });
        $scope.cancel();
    };
    $scope.disabled = undefined;
    $scope.searchEnabled = undefined;

    $scope.enable = function() {
        $scope.disabled = false;
    };

    $scope.disable = function() {
        $scope.disabled = true;
    };

    $scope.enableSearch = function() {
        $scope.searchEnabled = true;
    }

    $scope.disableSearch = function() {
        $scope.searchEnabled = false;
    }

    $scope.clear = function() {
        $scope.person.selected = undefined;
        $scope.address.selected = undefined;
        $scope.country.selected = undefined;
    };

    $scope.someGroupFn = function (item){

        if (item.name[0] >= 'A' && item.name[0] <= 'M')
            return 'From A - M';

        if (item.name[0] >= 'N' && item.name[0] <= 'Z')
            return 'From N - Z';

    };

    $scope.personAsync = {selected : "wladimir@email.com"};
    $scope.peopleAsync = [];

    $scope.counter = 0;
    $scope.someFunction = function (item, model){
        $scope.counter++;
        $scope.eventResult = {item: item, model: model};
    };

    $scope.removed = function (item, model) {
        $scope.lastRemoved = {
            item: item,
            model: model
        };
    };

    $scope.tagTransform = function (newTag) {
        var email = newTag.trim();

        if (email.match(/^[a-z][a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/i)) {
            var item = {
                email: newTag
            };
            return item;

        } else {
            return null;
        }
    };

    $scope.person = {};
    $scope.people = [
    ];

    $scope.multiEmail = {};
    $scope.multiEmail.selecaoEmail = [];
    $scope.multiEmail.emailSelecionado = $scope.multiEmail.selecaoEmail;
    $scope.addPerson = function(item, model){
        if(item.hasOwnProperty('isTag')) {
            delete item.isTag;
            $scope.people.push(item);
        }
    }
}