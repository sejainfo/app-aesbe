/**
 * Alerts Controller
 */

angular
    .module('RDash')
    .controller(
        'TarifasPrincipalCtrl',
        [
            '$scope',
            '$timeout',
            TarifasPrincipalCtrl
        ]
);

function TarifasPrincipalCtrl(
    $scope, $timeout
)
{
    $scope.groups = [
        {
            title: "Dynamic Group Header - 1",
            content: "Dynamic Group Body - 1",
            open: false,
            checked:true

        },
        {
            title: "Dynamic Group Header - 2",
            content: "Dynamic Group Body - 2",
            open: false,
            checked:false
        }
    ];

    //$scope.checkboxClick = function(index){
    //    $timeout(function () {
    //        $scope.groups[index].checked = $scope.groups[index].checked ? false : true;
    //    });
    //}

    $scope.checkboxClick = function(group, $event){
        $event.stopPropagation();
    }
}
