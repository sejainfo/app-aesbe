angular
    .module('RDash')
    .factory('EmpresaSrv', ['$http', 'config', EmpresaSrv]);

function EmpresaSrv($http, config) {

    var _getEmpresas = function () {
        return $http.get(config.baseUrl + '/publico/empresa');
    };

    var _carregarEmpresas = function () {
        //var itensEmpresa = [{
        //    "id": "",
        //    "label": "Selecione"
        //}];
        var empresas;
        _getEmpresas().success(function (data) {
            //console.log(data);
            empresas =  data._embedded.empresa
            //angular.forEach(data._embedded.empresa, function (item) {
            //    //itensEmpresa.push({
            //    //    "id": item.coEmpresa,
            //    //    "label": item.noEmpresa
            //    //});
            //    //empresas = {
            //    //    "values": itensEmpresa
            //    //};
            //});

        });
        return empresas;
    };

    return {
        getEmpresas: _getEmpresas,
        carregarEmpresas: _carregarEmpresas
    };


}