angular
    .module('RDash')
    .factory('MenuSrv', ['$http', 'config', '$localStorage', MenuSrv]);

function MenuSrv($http, config, $localStorage) {

    var _getMenuSuperior = function () {
        var menuSuperior;
        angular.forEach($localStorage.menu, function (itemMenu) {
            if ($localStorage.moduloAtivo == itemMenu.idModulo) {
                menuSuperior = itemMenu.funcionalidade;
            }
        }, menuSuperior);
        return menuSuperior;
    };
    var _setMenu = function () {
        if (!$localStorage.menu) {
            $http.get(config.baseUrl + '/publico/menu').success(function (data) {
                $localStorage.menu = data;
            });
        }
        //return $localStorage.menu;
    };

    return {
        setMenu: _setMenu,
        getMenuSuperior: _getMenuSuperior
    };


}