angular
    .module('RDash')
    .factory('OauthUsersSrv', ['$http', 'config', '$resource', OauthUsersSrv]);

function OauthUsersSrv($http, config, $resource) {

    var _getUsuarios = function () {
        return $resource(
            config.baseUrl + '/configuracoes/oauth-users'
        ).get();
    };
    var _getUriUsuarios = function () {
        return  config.baseUrl + '/configuracoes/oauth-users';
    };

    return {
        getUsuarios: _getUsuarios,
        getUriUsuarios: _getUriUsuarios,
    };


}