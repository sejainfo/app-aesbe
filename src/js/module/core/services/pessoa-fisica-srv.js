angular
    .module('RDash')
    .factory('PessoaFisicaSrv', ['$http', 'config', 'inform', '$resource', 'OauthUsersSrv', PessoaFisicaSrv]);

function PessoaFisicaSrv($http, config, inform, $resource, OauthUsersSrv) {

    var _ativarUsuario = function (coPessoa) {
        var pessoaFisica = {
            stAtivo: '1'
        }
        _updatePessoa(coPessoa, pessoaFisica).success(function() {
            inform.add('Usuário ativado com sucesso', {
                "type": "success",
                "ttl": 9000,
            });
        });

    };
    var _inativarUsuario = function (coPessoa) {
        var pessoaFisica = {
            stAtivo: '0'
        }
        _updatePessoa(coPessoa, pessoaFisica).success(function() {
            inform.add('Usuário inativado com sucesso', {
                "type": "success",
                "ttl": 9000,
            });
        });

    };
    var _updatePessoa = function (coPessoa, pessoaFisica) {
        return $http.put(config.baseUrl + '/core/pessoa-fisica/'+coPessoa, pessoaFisica);
    };
    return {
        updatePessoa: _updatePessoa,
        inativarUsuario: _inativarUsuario,
        ativarUsuario: _ativarUsuario,
    };

}