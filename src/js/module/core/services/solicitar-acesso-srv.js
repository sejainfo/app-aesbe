angular
    .module('RDash')
    .factory('SolicitarAcessoSrv', ['$http', 'config', SolicitarAcessoSrv]);

function SolicitarAcessoSrv($http, config) {


    var _solicitarAcesso = function (dadosAcesso) {
        return $http.post(config.baseUrl + '/publico/solicitar-acesso', dadosAcesso);
    };

    return {
        solicitarAcesso: _solicitarAcesso,
    };


}