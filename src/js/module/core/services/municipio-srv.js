angular
    .module('RDash')
    .factory('MunicipioSrv', ['$http', 'config', MunicipioSrv]);

function MunicipioSrv($http, config)
{
    var _getAllMunicipio = function (uf) {
        return $http.get(config.baseUrl + '/publico/municipio?noUf='+uf);
    }

    return {
        getAllMunicipio: _getAllMunicipio
    };
}