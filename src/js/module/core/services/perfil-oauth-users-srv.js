angular
    .module('RDash')
    .factory('PerfilOauthUsersSrv', ['$http', 'config', '$resource', PerfilOauthUsersSrv]);

function PerfilOauthUsersSrv($http, config, $resource) {

    var _vincularPerfil = function (coUsuario, coPerfil) {
        var dados = {
            coPerfil: coPerfil,
            coUsuario: coUsuario,
        }
        return $http.post(config.baseUrl + '/core/perfil-oauth-users', dados);
    };
    return {
        vincularPerfil: _vincularPerfil,
    };
}