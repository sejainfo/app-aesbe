angular
    .module('RDash')
    .factory('DadosSessaoSrv', ['$http', 'config', '$resource', DadosSessaoSrv]);

function DadosSessaoSrv($http, config, $resource) {

    var _getDadosSessao = function () {
        return $http.get(config.baseUrl + '/publico/dados-sessao');
    };

    return {
        getDadosSessao: _getDadosSessao,
    };


}
