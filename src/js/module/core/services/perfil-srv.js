angular
    .module('RDash')
    .factory('PerfilSrv', ['$http', 'config', PerfilSrv]);

function PerfilSrv($http, config) {

    var _getPerfis = function () {
        return $http.get(config.baseUrl + '/core/perfil');
    };

    var _carregarPerfis = function () {
        var perfis;
        _getPerfis().success(function (data) {
            perfis =  data._embedded.perfil

        });
        return perfis;
    };

    return {
        getPerfis: _getPerfis,
        carregarPerfis: _carregarPerfis
    };


}