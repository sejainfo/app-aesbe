angular
    .module('RDash')
    .factory('UfSrv', ['$http', 'config', Ufsrv]);

function Ufsrv($http, config)
{
    var _getAllUf = function () {
        return $http.get(config.baseUrl + '/publico/uf')
    }

    return {
        getAllUf: _getAllUf
    };
}