/**
 * Login Controller
 */

angular
    .module('RDash')
    .controller('MenuCtrl', ['$scope', 'MenuSrv', '$localStorage', 'DadosSessaoSrv', MenuCtrl]);

function MenuCtrl($scope, MenuSrv, $localStorage, DadosSessaoSrv) {

    MenuSrv.setMenu();
    $scope.menuModulo = function(){
        return $localStorage.menu;
    };
    //$scope.dsModuloAtivo = function(){
    //    return $localStorage.dsModuloAtivo
    //};
    $scope.menuSuperior = function(){
        return MenuSrv.getMenuSuperior();
    };
    $scope.ativarMenuSuperior = function(idModulo, noModuloVisivel){
        //console.log(noModuloVisivel);
        $localStorage.moduloAtivo = idModulo;
        //$localStorage.dsModuloAtivo = noModuloVisivel;
        //$scope.dsModuloAtivo = $localStorage.dsModuloAtivo;
        //$scope.dsModuloAtivo = $localStorage.dsModuloAtivo;
        $scope.menuSuperior = function(){
            return MenuSrv.getMenuSuperior()
        };
    };

    $scope.dadosSessao = DadosSessaoSrv.getDadosSessao().success(function(data){
        data._embedded.dados_sessao.forEach(function(dadosSessao){
            $scope.dadosSessao = dadosSessao;
        });
    });

    $scope.logout = function(){
        document.cookie = 'token' + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
        delete $localStorage.menu;
    };
}