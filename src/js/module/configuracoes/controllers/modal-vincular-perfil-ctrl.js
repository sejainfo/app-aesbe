angular
    .module('RDash')
    .controller('ModalVincularPerfilCtrl',
        [
            '$scope', '$uibModalInstance', 'PerfilSrv', '$controller', 'inform','coUsuario','row',
            'PerfilOauthUsersSrv', 'OauthUsersSrv','$resource',
            ModalVincularPerfilCtrl
        ]);

function ModalVincularPerfilCtrl($scope, $uibModalInstance, PerfilSrv, $controller, inform, coUsuario, row,
                                 PerfilOauthUsersSrv, OauthUsersSrv, $resource) {
    PerfilSrv.getPerfis().success(function (data) {
        $scope.perfis = data._embedded.perfil
    });
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.vincularPerfilSubmit = function(item){
        PerfilOauthUsersSrv.vincularPerfil(coUsuario, item.perfil.coPerfil)
            .success(function() {
                $uibModalInstance.dismiss('cancel');
                inform.add('Perfil vinculado com sucesso', {
                    "type": "success",
                    "ttl": 9000,
                });
                //console.log($scope.dtInstance);return;
                $scope.dtInstance.changeData($resource(OauthUsersSrv.getUriUsuarios()).query().$promise);
                //$scope.dtInstance.reloadData();
            });
    };
}