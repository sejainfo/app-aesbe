/**
 * Alerts Controller
 */

angular
    .module('RDash')
    .controller(
        'UsuarioModerarAcessoCtrl',
        [
            '$scope', 'PessoaFisicaSrv', '$uibModal', '$controller', 'OauthUsersSrv',
            'DTOptionsBuilder', 'DTColumnBuilder','$resource', 'inform',
            UsuarioModerarAcessoCtrl
        ]
);

function UsuarioModerarAcessoCtrl(
    $scope, PessoaFisicaSrv, $uibModal, $controller, OauthUsersSrv, DTOptionsBuilder, DTColumnBuilder, $resource, inform
)
{
    $scope.dtInstance = {};
    angular.extend($controller('AbstractCtrl', {$scope: $scope}));
        $scope.dtColumns = [
            DTColumnBuilder.newColumn('noPessoa').withTitle('Nome'),
            DTColumnBuilder.newColumn('dsEmail').withTitle('E-mail'),
            DTColumnBuilder.newColumn('noEmpresa').withTitle('Empresa'),
            DTColumnBuilder.newColumn('stAtivo').withTitle('Ativo').renderWith(function (stAtivo, type) {
                if (stAtivo) {
                    return 'Sim';
                }
                return 'Não';
            }),
            DTColumnBuilder.newColumn('noPerfil').withTitle('Perfil').renderWith(function (noPerfil, type) {
                if (!noPerfil) {
                    return 'Sem Perfil';
                }
                return noPerfil;
            }),
            DTColumnBuilder.newColumn(null).withOption('className', 'col-md-3').withTitle('Ação').notSortable()
                .renderWith(actionsHtml)
        ];
        function actionsHtml(data, type, full, meta) {
            var html =
                '<button title="Vincular Perfil" alt="Vincular Perfil" class="btn btn-warning vincularPerfil">' +
                '   <i class="fa fa-edit"></i>' +
                'Vincular Perfil</button>&nbsp;';
            if (data.stAtivo) {
                html +=
                    '<button data-id="' + data.coPessoa + '" title="Inativar" alt="Ativar" class="btn btn-danger inativar">' +
                    'Inativar' +
                    '</button>';
            } else {
                html +=
                    '<button data-id="' + data.coPessoa + '" title="Ativar" alt="Ativar" class="btn btn-primary ativar">' +
                    'Ativar' +
                    '</button>';
            }
            return html;
        }

        $scope.dtOptions = DTOptionsBuilder
        $scope.dtOptions = DTOptionsBuilder.fromFnPromise(function () {
                return $resource(OauthUsersSrv.getUriUsuarios()).query().$promise
            })
            .withPaginationType('simple_numbers')
            .withOption($scope.option)
            .withOption('rowCallback', rowCallback)
            .withLanguage($scope.optionLanguage);
    function rowCallback(tabRow, aData, iDataIndex) {
        $('td .inativar', tabRow).bind('click', function() {
            var pessoaFisica = {
                stAtivo: '0'
            }
            PessoaFisicaSrv.updatePessoa(aData.coPessoa, pessoaFisica).success(function() {
                inform.add('Usuário inativado com sucesso', {
                    "type": "success",
                    "ttl": 9000,
                });
                $scope.dtInstance.changeData($resource(OauthUsersSrv.getUriUsuarios()).query().$promise);
                $scope.dtInstance.reloadData();
            });
        });
        $('td .ativar', tabRow).bind('click', function() {
            var pessoaFisica = {
                stAtivo: '1'
            }
            PessoaFisicaSrv.updatePessoa(aData.coPessoa, pessoaFisica).success(function() {
                inform.add('Usuário ativado com sucesso', {
                    "type": "success",
                    "ttl": 9000,
                });
                $scope.dtInstance.changeData($resource(OauthUsersSrv.getUriUsuarios()).query().$promise);
                $scope.dtInstance.reloadData();
            });
        });
        $('td .vincularPerfil', tabRow).bind('click', function() {
            var tr = $(tabRow);
            $scope.$apply(function() {
                $scope.openModalVincularPerfil(aData.coUsuario, tr);
            });
        });
    }

    $scope.openModalVincularPerfil = function (coUsuario, row) {
        $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'modalVincularPerfil.html',
            controller: 'ModalVincularPerfilCtrl',
            scope: $scope,
            resolve: {
                coUsuario: coUsuario,
                row: row,
            }
        });

    };

}
