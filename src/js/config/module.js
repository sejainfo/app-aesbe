angular.module('RDash',
    [
        'ui.bootstrap', 'ui.router', 'ngCookies', 'angular-oauth2', 'localytics.directives',
        'ui.utils.masks', 'inform', 'ngResource', 'datatables', 'ui.bootstrap',
        'datatables.bootstrap', 'angular-loading-bar', 'ngAnimate', 'rt.popup', 'angularFileUpload',
        'anguFixedHeaderTable', 'ngStorage', 'ngSanitize', 'br-filters', 'AngularPrint', 'ui.select'
    ]
);
