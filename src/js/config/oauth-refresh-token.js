angular.module('RDash')
	.run(['$rootScope', '$window', 'OAuth', function($rootScope, $window, OAuth) {
		console.log($rootScope.$on('oauth:error'));
		$rootScope.$on('oauth:error', function(event, rejection) {
			console.log('aqui');
			// Ignore `invalid_grant` error - should be catched on `LoginController`.
			if ('invalid_grant' === rejection.data.error) {
				return;
			}

			// Refresh token when a `invalid_token` error occurs.
			if ('invalid_token' === rejection.data.error) {
                console.log('refresh');
				return OAuth.getRefreshToken();
			}

			// Redirect to `/login` with the `error_reason`.
			return $window.location.href = '/login?error_reason=' + rejection.data.error;
		});
	}]);