'use strict';

/**
 * Route configuration for the RDash module.
 */
angular.module('RDash').config(['$stateProvider', '$urlRouterProvider',
    function ($stateProvider, $urlRouterProvider) {

        var bancoPreco = {
            name: 'Banco de Preços',
            icon: 'fa-bank'
        };

        var configuracoes = {
            name: 'Configurações',
            icon: 'fa-cog'
        };

        var tarifas = {
            name: 'Tarifas',
            icon: 'fa-paypal'
        };

        // For unmatched routes
        $urlRouterProvider.otherwise('/');

        // Application routes
        $stateProvider
            .state('index', {
                url: '/',
                templateUrl: 'templates/login.html',
                controller: 'LoginCtrl',
                params: {
                    activetab: bancoPreco
                }
            })
            .state('bancoPreco', {
                url: '/banco-preco',
                abstract: true,
                templateUrl: 'templates/dashboard.html',
                controller: 'MenuCtrl',
                controllerAs: "vm",
                params: {
                    activetab: bancoPreco
                }
            })
            .state('configuracoes', {
                url: '/configuracoes',
                abstract: true,
                templateUrl: 'templates/dashboard.html',
                controller: 'MenuCtrl',
                controllerAs: "vm",
                params: {
                    activetab: configuracoes
                }
            })
            .state('tarifas', {
                url: '/tarifas',
                abstract: true,
                templateUrl: 'templates/dashboard.html',
                controller: 'MenuCtrl',
                controllerAs: "vm",
                params: {
                    activetab: tarifas
                }
            })
            .state('primeiroAcesso', {
                url: '/primeiro-acesso',
                templateUrl: 'templates/primeiro-acesso.html',
                controller: 'PrimeiroAcessoCtrl'
            });

        $stateProvider.state('bancoPreco.principal', {
            url: '/principal',
            views: {
                'banco-preco-principal': {
                    templateUrl: 'templates/banco-preco/principal.html',
                    controller: "BancoPrecoPrincipalCtrl"
                }
            }
        });
        $stateProvider.state('bancoPreco.propostaMaterialPrincipal', {
            url: '/proposta-material/principal',
            views: {
                'proposta-material': {
                    templateUrl: 'templates/banco-preco/proposta-material/principal.html',
                    controller: "PropostaMaterialPrincipalCtrl"
                }
            }
        });
        $stateProvider.state('bancoPreco.propostaMaterialImportarDados', {
            url: '/proposta-material/importar-dados',
            views: {
                'proposta-material': {
                    templateUrl: 'templates/banco-preco/proposta-material/importar-dados.html',
                    controller: "PropostaMaterialImportarDadosCtrl"
                }
            }
        });
        $stateProvider.state('bancoPreco.propostaMaterialListaImportados', {
            url: '/proposta-material/lista-importados',
            views: {
                'proposta-material': {
                    templateUrl: 'templates/banco-preco/proposta-material/lista-importados.html',
                    controller: "PropostaMaterialListaImportadosCtrl"
                }
            }
        });
        $stateProvider.state('bancoPreco.propostaMaterialListar', {
            url: '/proposta-material/listar',
            views: {
                'proposta-material': {
                    templateUrl: 'templates/banco-preco/proposta-material/listar.html',
                    controller: "PropostaMaterialListarCtrl"
                }
            }
        });
        $stateProvider.state('bancoPreco.propostaMaterialFormulario', {
            url: '/proposta-material/formulario/:id',
            views: {
                'proposta-material': {
                    templateUrl: 'templates/banco-preco/proposta-material/formulario.html',
                    controller: "PropostaMaterialFormularioCtrl"
                }
            }
        });
        $stateProvider.state('bancoPreco.propostaMaterialDetalhar', {
            url: '/proposta-material/detalhar/:id',
            views: {
                'proposta-material': {
                    templateUrl: 'templates/banco-preco/proposta-material/detalhar.html',
                    controller: "PropostaMaterialDetalharCtrl"
                }
            }
        });
        $stateProvider.state('bancoPreco.propostaMaterialDetalharProposta', {
            url: '/proposta-material/detalhar-proposta/:id',
            views: {
                'proposta-material': {
                    templateUrl: 'templates/banco-preco/proposta-material/detalhar-proposta.html',
                    controller: "PropostaMaterialDetalharPropostaCtrl"
                }
            }
        });
        $stateProvider.state('bancoPreco.materialPrincipal', {
            url: '/material/principal',
            views: {
                'proposta-material': {
                    templateUrl: 'templates/banco-preco/material/principal.html',
                    controller: "MaterialPrincipalCtrl"
                }
            }
        });
        $stateProvider.state('bancoPreco.fornecedorPrincipal', {
            url: '/fornecedor/principal',
            views: {
                'proposta-material': {
                    templateUrl: 'templates/banco-preco/fornecedor/principal.html',
                    controller: "FornecedorPrincipalCtrl"
                }
            }
        });
        $stateProvider.state('configuracoes.usuarioModerarAcesso', {
            url: '/usuario/moderar-acesso',
            views: {
                'proposta-material': {
                    templateUrl: 'templates/configuracoes/usuario/moderar-acesso.html',
                    controller: "UsuarioModerarAcessoCtrl"
                }
            }
        });
        $stateProvider.state('tarifas.principal', {
            url: '/principal',
            views: {
                'proposta-material': {
                    templateUrl: 'templates/tarifas/principal.html',
                    controller: "TarifasPrincipalCtrl"
                }
            }
        });
    }
]);