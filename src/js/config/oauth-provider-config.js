angular.module("RDash").config(['OAuthProvider', function (OAuthProvider) {
	OAuthProvider.configure({
		//baseUrl: 'http://api.aesbe.web2413.uni5.net/public',
		baseUrl: 'http://dev.aesbe.local',
		clientId: 'app_aesbe',
		clientSecret: 'app_aesbe',
		grantPath: '/oauth',
		revokePath: '/oauth'
	});
}]);


angular.module("RDash").config(['$httpProvider', function ($httpProvider) {
    var interceptor = ['$q', '$window', 'inform', '$location', '$injector', '$timeout',
        function($q, $window, inform, $location, $injector, $timeout) {
        return {
            request: function (config) {
                config.headers = config.headers || {};
                if ($window.sessionStorage.token) {
                    config.headers.Authorization = 'Bearer ' + $window.sessionStorage.token;
                }
                return config;
            },

            requestError: function(rejection) {
                return $q.reject(rejection);
            },

            response: function (response) {
                return response || $q.when(response);
            },
            responseError: function(rejection) {
                if (rejection != null && (rejection.status === 401 || rejection.status === 403)) {
                    //$location.path("/login/1");
                    //$location.path("/banco-preco/proposta-material/principal");
                    inform.add("Usuário não autorizado", {
                        "type": "danger"
                    });
                    $timeout(function(){
                        $location.path("/login");
                    }, 3000);
                }
                return $q.reject(rejection);
            }
        };
    }];
    $httpProvider.interceptors.push(interceptor);
}]);