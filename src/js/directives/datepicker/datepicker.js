'use strict';

/**
 * @ngdoc directive
 * @name izzyposWebApp.directive:adminPosHeader
 * @description
 * # adminPosHeader
 */

angular.module('RDash')
    .directive('myDatepicker',function(){
        return {
            restrict: 'A',
            scope: {
                model: "=",
                format: "@",
                options: "=datepickerOptions",
                myid: "@",
                myname: "@",
                myrequired: "@"
            },
            template:'<p class="input-group"><input type="text" class="form-control" id="{{myid}}" name="{{myname}}" uib-datepicker-popup="{{format}}" ng-model="model" is-open="opened" datepicker-options="dateOptions" ui-date-format="mm/dd/yyyy" ng-required="{{myrequired}}" close-text="Close"  /><span class="input-group-btn"><button type="button" class="btn btn-default" ng-click="open($event)"><i class="glyphicon glyphicon-calendar"></i></button></span></p>',
            link: function(scope, element) {
                scope.popupOpen = false;
                scope.openPopup = function($event) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    scope.popupOpen = true;
                };

                scope.open = function($event) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    scope.opened = true;
                };

            },
        };
    });