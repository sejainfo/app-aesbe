/**
 * Master Controller
 */

angular.module('RDash')
    .controller('MasterCtrl', ['$scope', '$cookieStore', 'config', '$state', MasterCtrl]);

function MasterCtrl($scope, $cookieStore, config, $state) {

    $scope.baseRest = config.baseUrl;
    /**
     * Sidebar Toggle & Cookie Control
     */
    var mobileView = 992;

    // $cookieStore.remove('noModuloAtivo');
    // $cookieStore.remove('dsIconAtivo');

    console.log();

    $scope.dsModuloAtivo = function(){
        return $state.params.activetab.name;
    };
    $scope.dsIconAtivo = function(){
        return $state.params.activetab.icon;
    };

    $scope.getWidth = function() {
        return window.innerWidth;
    };

    $scope.$watch($scope.getWidth, function(newValue, oldValue) {
        if (newValue >= mobileView) {
            if (angular.isDefined($cookieStore.get('toggle'))) {
                $scope.toggle = ! $cookieStore.get('toggle') ? false : true;
            } else {
                $scope.toggle = true;
            }
        } else {
            $scope.toggle = false;
        }

    });

    $scope.toggleSidebar = function() {
        $scope.toggle = !$scope.toggle;
        $cookieStore.put('toggle', $scope.toggle);
    };

    window.onresize = function() {
        $scope.$apply();
    };
}