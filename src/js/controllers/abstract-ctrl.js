/**
 * Alerts Controller
 */

angular
    .module('RDash')
    .controller('AbstractCtrl', ['$scope', 'DTOptionsBuilder', 'DTColumnBuilder', '$resource', '$filter', 'inform', AbstractCtrl]);

function AbstractCtrl($scope, DTOptionsBuilder, DTColumnBuilder, $resource, $filter, inform) {
    var vm = $scope;
    vm.isCollapsed = true;
    vm.alerts = [];
    $scope.directiveOptions = {
        no_results_text: "Nenhum resultado para",
        placeholder_text_single: "Selecionar e Opção"
    };
    $scope.animationsEnabled = false;
    vm.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    };
    vm.tratamentoErrosAlert = function (data) {
        if (data.status != 401) {
            angular.forEach(data.validation_messages, function (mensagem) {
                $scope.alerts.push({
                    type: 'danger',
                    msg: mensagem
                });
            });
        }
    }
    $scope.tratamentoErros = function (data) {
        if (data.status != 401) {
            if (data.detail){
                inform.add(data.detail, {
                    "type": "danger",
                    //"ttl": 9000,
                });
            }
            angular.forEach(data.validation_messages, function (mensagem) {
                angular.forEach(mensagem, function (msg) {
                    inform.add(msg, {
                        "type": "danger",
                        //"ttl": 9000,
                    });
                })
            });
        }
    }
    $scope.option = {
        "bPaginate": true,
        "bLengthChange": true,
        "bFilter": true,
        "bFixedHeader": true,
        "bSort": true,
        "bInfo": true,
        "bAutoWidth": true,
        "bStateSave": true
    };
    $scope.optionLanguage = {
        "sProcessing": "Processando...",
        "sLengthMenu": "Mostrar _MENU_ registros por página",
        "sZeroRecords": "Nenhum Registro Encontrado...",
        "sInfo": "Paginação _START_ de _END_, Total _TOTAL_ registros",
        "sInfoEmpty": "Paginação 0 de 0, Total 0 registros",
        "sInfoFiltered": "(Filtrado de _MAX_ registros ao todo)",
        "sSearch": "Procurar: ",
        "sNext": "Próximo",
        "sLoadingRecords": "carregando...",
        "sEmptyTable": "Nenhum Registro Encontrado",
        "oPaginate": {
            "sPrevious": "Anterior",
            "sNext": "Próximo",
            "sFirst": "Primeira",
            "sLast": "Última",
            "sZeroRecords": "Nenhum Registro Encontrado..."
        }
    };
    $scope.format = 'dd/MM/yyyy';

    //$scope.dtInstance = {};
    $scope.pesquisarPrecoMaterial = function(resource, action) {
        if (!resource){
            resource = '/templates/data.json';
        }
        $scope.dtColumns = [
            DTColumnBuilder.newColumn(null).withTitle('').withOption('className', 'details-control').notSortable()
                .renderWith(function (data, type, full, meta) {
                    return '';
                }),
            DTColumnBuilder.newColumn('co_material').withTitle('Código AESBE'),
            DTColumnBuilder.newColumn('noBasico').withTitle('Nome Básico'),
            DTColumnBuilder.newColumn('vl_com_imposto').withTitle('Preço').renderWith(function (valor, type) {
                return $filter('currency')(valor, 'R$', 4); //date filter
            }),
            DTColumnBuilder.newColumn('qt_produto').withTitle('Quantidade').renderWith(function (quantidade, type) {
                return $filter('number')(quantidade); //date filter
            }),
            DTColumnBuilder.newColumn('dt_informacao').withTitle('Data Informação').renderWith(function (data, type) {
                return $filter('date')(data, 'dd/MM/yyyy'); //date filter
            }),


        ];
        if (action) {
            //$scope.dtColumns
            $scope.dtColumns.push(
                DTColumnBuilder.newColumn(null).withOption('className', 'col-md-2').withTitle('Ação').notSortable()
                    .renderWith(actionsHtml)
            );
        }

        $scope.dtOptions = DTOptionsBuilder
            .fromFnPromise(function() {
                return $resource(resource).query().$promise;
            })
            .withPaginationType('simple_numbers')
            .withOption($scope.option)
            .withOption('rowCallback', rowCallback)
            //.withOption('responsive', true)
            .withLanguage($scope.optionLanguage);
        function format (d) {
            var stEespecificacaoPadrao = d.st_especificacao_padrao == 'S' ? 'SIM' : 'NÃO';
            var espClasse = d.ds_esp_classe ? d.ds_esp_classe : '';
            var espSerie = d.ds_esp_serie ? d.ds_esp_serie : '';
            var nuSabesp = d.nu_sabesp ? d.nu_sabesp : '';
            var vlIiss = d.vl_iss ? d.vl_iss : '';
            var dsObservacao = d.ds_observacao ? d.ds_observacao : '';
            var dtInformacao = $filter('date')(d.dt_informacao, 'dd/MM/yyyy');
            var cnpj = $filter('cnpj')(d.nu_cnpj);
            var dtProposta = $filter('date')(d.dt_proposta, 'dd/MM/yyyy');
            var dtContratacao = $filter('date')(d.dt_contratacao, 'dd/MM/yyyy');
            var dtVigencia = $filter('date')(d.dt_vigencia_contrato, 'dd/MM/yyyy');
            return '<script type="text/javascript">$(".nav-tabs a").click(function (e) {' +
                'e.preventDefault();' +
                '$(this).tab("show");' +
                '});</script>' +
                '<ul class="nav nav-tabs">' +
                '<li class="active"><a href="#material' + d.co_proposta_dados + '" data-toggle="tab" aria-expanded="true">Detalhes do Material</a></li>' +
                '<li><a href="#preco' + d.co_proposta_dados + '" data-toggle="tab">Detalhes do Preço</a></li>' +
                '</ul>' +
                '<div class="tab-content text-justify" id="tabs" style="margin-top: 20px; line-height: 1.6;">' +
                '<div class="tab-pane active " id="material' + d.co_proposta_dados + '">' +
                '<div class="col-md-12">' +
                '<label class="label-preco"><strong>Código SABESP: </strong></label>' +
                '<span class="span-preco">' + nuSabesp + '</span>' +
                '<br /></div>' +
                '<div class="col-md-12">' +
                '<label class="label-preco"><strong>Unidade de Medida: </strong></label>' +
                '<span class="span-preco">' + d.no_unidade_medida + '</span>' +
                '<br /></div>' +
                '<div class="col-md-12">' +
                '<label class="label-preco"><strong>Especificação de Classe: </strong></label>' +
                '<span class="span-preco">' + espClasse + '</span>' +
                '<br /></div>' +
                '<div class="col-md-12">' +
                '<label class="label-preco"><strong>Especificação de Série: </strong></label>' +
                '<span class="span-preco">' + espSerie + '</span>' +
                '<br /><br /></div>' +
                '</div>' +
                '<div class="tab-pane" id="preco' + d.co_proposta_dados + '">' +
                '<div class="col-md-12">' +
                '<label class="label-preco"><strong>Nome da Empresa de Saneamento: </strong></label>' +
                '<span class="span-preco">' + d.no_empresa + '</span>' +
                '<br /><br /></div>' +
                '<div class="col-md-12">' +
                '<label class="label-preco"><strong>Nome Fornecedor: </strong></label>' +
                '<span class="span-preco">' + d.no_fornecedor + '</span>' +
                '</div>' +
                '<div class="col-md-12">' +
                '<label class="label-preco"><strong>CNPJ Fornecedor: </strong></label>' +
                '<span class="span-preco" >' + cnpj+ '</span>' +
                '</div>' +
                '<div class="col-md-12">' +
                '<label class="label-preco"><strong>Data da Proposta: </strong></label>' +
                '<span class="span-preco">' + dtProposta + '</span>' +
                '</div>' +
                '<div class="col-md-12">' +
                '<label class="label-preco"><strong>Número do Contrato: </strong></label>' +
                '<span class="span-preco">' + d.nu_contrato+ '</span>' +
                '</div>' +
                '<div class="col-md-12">' +
                '<label class="label-preco"><strong>Número ATA registro de preços: </strong></label>' +
                '<span class="span-preco">' + d.nu_ata+ '</span>' +
                '</div>' +
                '<div class="col-md-12">' +
                '<label class="label-preco"><strong>Modalidade: </strong></label>' +
                '<span class="span-preco">' + d.ds_modalidade+ '</span>' +
                '</div>' +
                '<div class="col-md-12">' +
                '<label class="label-preco"><strong>Data da Contratação: </strong></label>' +
                '<span class="span-preco">' + dtContratacao+ '</span>' +
                '</div>' +
                '<div class="col-md-12">' +
                '<label class="label-preco"><strong>Data Vigência do Contrato: </strong></label>' +
                '<span class="span-preco">' + dtVigencia+ '</span>' +
                '</div>' +
                '<div class="col-md-12">' +
                '<label class="label-preco"><strong>Condições de Pagamento: </strong></label>' +
                '<span class="span-preco">' + d.qt_dias_pagamento+ ' dias</span>' +
                '</div>' +
                '<div class="col-md-12">' +
                '<label class="label-preco"><strong>FOB/CIF: </strong></label>' +
                '<span class="span-preco">' + d.st_fob_cif+ '</span>' +
                '</div>' +
                '<div class="col-md-12">' +
                '<label class="label-preco"><strong>IPI: </strong></label>' +
                '<span class="span-preco">' + d.vl_ipi+ '%</span>' +
                '</div>' +
                '<div class="col-md-12">' +
                '<label class="label-preco"><strong>ICMS: </strong></label>' +
                '<span class="span-preco">' + d.vl_icms+ '%</span>' +
                '</div>' +
                '<div class="col-md-12">' +
                '<label class="label-preco"><strong>ISS: </strong></label>' +
                '<span class="span-preco">' + vlIiss+ '%</span>' +
                '</div>' +
                '<div class="col-md-12">' +
                '<label class="label-preco"><strong>Condições de Fornecimento: </strong></label>' +
                '<span class="span-preco">' + d.st_condicao_fornecimento+ '</span>' +
                '</div>' +
                '<div class="col-md-12">' +
                '<label class="label-preco"><strong>Tipo de Inspeção: </strong></label>' +
                '<span class="span-preco">' + d.tp_inspecao+ '</span>' +
                '</div>' +
                '<div class="col-md-12">' +
                '<label class="label-preco"><strong>Nivel de Atendimento: </strong></label>' +
                '<span class="span-preco">' + d.st_nivel_atendimento+ '</span>' +
                '</div>' +
                '<div class="col-md-12">' +
                '<label class="label-preco"><strong>Especificação Padrão: </strong></label>' +
                '<span class="span-preco">' + stEespecificacaoPadrao+ '</span>' +
                '</div>' +
                '<div class="col-md-12">' +
                '<label class="label-preco"><strong>Observação: </strong></label>' +
                '<span class="span-preco">' + dsObservacao+ '</span>' +
                '</div>' +
                '<div class="col-md-12">' +
                '<label class="label-preco"><strong>Data da Informação: </strong></label>' +
                '<span class="span-preco">' + dtInformacao+ '</span>' +
                '<br /><br /></div>' +
                '</div>' +
                '</div>';
        }
        function actionsHtml(data, type, full, meta) {
            return '<a href="/#/banco-preco/proposta-material/formulario/'+data.co_proposta_dados+'"><button title="Editar" alt="Editar" class="btn btn-warning">' +
                '   <i class="fa fa-edit"></i>' +
                '</button></a>&nbsp;' +
                '<button data-id="'+data.co_proposta_dados+'" title="Excluir" alt="Excluir" class="btn btn-danger excluirPropostaDados" ng-click="excluirPropostaDados()">' +
                '   <i class="fa fa-trash-o"></i>' +
                '</button>';
        }
        function rowCallback(tabRow, aData, iDataIndex) {
            $('td.details-control', tabRow).unbind('click');
            $('td.details-control', tabRow).on('click', function () {
                var tr = $(tabRow);
                var table = $scope.dtInstance.DataTable;
                var row = table.row(tr);
                if ( row.child.isShown() ) {
                    row.child.hide();
                    tr.removeClass('shown');
                } else {
                    row.child(format(row.data()) ).show();
                    tr.addClass('shown');
                }
            });
            $('td .excluirPropostaDados', tabRow).bind('click', function() {
                var tr = $(tabRow);
                //$(tr).remove();
                var table = $scope.dtInstance.DataTable;
                var row = table.row(tr);
                $scope.$apply(function() {
                    $scope.excluirPropostaDados = !$scope.excluirPropostaDados;
                    $scope.openModalProposta(aData.co_proposta_dados, tr);
                });
            });
        }
    }
    $scope.addAlert = function() {
        $scope.alerts.push({
            msg: 'Another alert!'
        });
    };

    $scope.closeAlert = function(index) {
        $scope.alerts.splice(index, 1);
    };

    $scope.formatDate = function (date) {
        var dtFormat = (date.date.replace("00:00:00.000000", "")).replace(" ", "");
        return new Date(dtFormat.replace( /(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3") );
    }
}