/**
 * Login Controller
 */

angular
    .module('RDash')
    .controller('LoginCtrl', ['$scope', '$state', 'OAuth', 'inform', '$stateParams',
        '$localStorage', LoginCtrl]);

function LoginCtrl($scope, $state, OAuth, inform, $stateParams, $localStorage) {

    if($stateParams.id == 1){
        inform.add("Usuário não autorizado", {
            "type": "danger"
        });
    }
    $scope.logar = function (login) {
        OAuth.getAccessToken(login).then(function () {
            $localStorage.moduloAtivo = 1;
            $localStorage.dsModuloAtivo = 'Banco de Preços';
            $state.go('bancoPreco.propostaMaterialPrincipal');

        }, function () {
            inform.add('Usuário e / ou senha inválido(s)', {
                "type": "danger",
                "ttl": 9000,
            });
        });
    };
}