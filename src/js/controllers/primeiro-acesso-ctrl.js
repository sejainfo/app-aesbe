/**
 * Login Controller
 */

angular
    .module('RDash')
    .controller('PrimeiroAcessoCtrl', ['$scope', 'inform', 'EmpresaSrv', 'SolicitarAcessoSrv', 'UfSrv','$controller',
        'MunicipioSrv','$state',
        PrimeiroAcessoCtrl]);

function PrimeiroAcessoCtrl($scope, inform, EmpresaSrv, SolicitarAcessoSrv, UfSrv, $controller, MunicipioSrv, $state) {
    angular.extend($controller('AbstractCtrl', {$scope: $scope}));

    $scope.municipios = [];
    $scope.acesso = {};
    $scope.solicitarPrimeiroAcesso = function (acesso) {
        if (!acesso) {
            return;
        }
        var dadosAcesso = acesso;
        dadosAcesso.coUf = acesso.uf.coUf;
        dadosAcesso.coMunicipio = acesso.municipio.coMunicipio;
        if (acesso.coEmpresa) {
            dadosAcesso.coEmpresa = acesso.coEmpresa.coEmpresa;
        }
        SolicitarAcessoSrv.solicitarAcesso(dadosAcesso)
            .success(function () {
                inform.add("Solicitação realizada com sucesso", {
                    "type": "success"
                });
                $scope.acesso = {};
            })
            .error(function (data) {
                $scope.tratamentoErros(data);
            });
    };

    EmpresaSrv.getEmpresas().success(function (data) {
        $scope.empresas = data._embedded.empresa
    });

    UfSrv.getAllUf().success(function (data) {
        $scope.ufs = data._embedded.uf;
    });

    $scope.setMunicipio = function(sgUf){
        //console.log(sgUf);
        //return;
        MunicipioSrv.getAllMunicipio(sgUf).success(function(data){
            $scope.municipios = data._embedded.municipio;
            console.log($scope.municipios);
        });
    };
}